import datetime
import random
import os

os.environ['DJANGO_SETTINGS_MODULE'] = "hcajax.settings"

from hc.models import data

now = datetime.datetime.now()

num_entries = 500

start_date = now - datetime.timedelta(seconds = num_entries * 20)

for i in range(num_entries):
    d = data(
        timestamp = start_date + datetime.timedelta(seconds = i*20),
        mc01 = random.random() * 965.126,
        mc02 = random.random() * 965.126,
        mc03 = random.random() * 965.126,
        mc04 = random.random() * 965.126,
        mc05 = random.random() * 965.126,
        mc06 = random.random() * 965.126,
        mc07 = random.random() * 965.126,
        mc08 = random.random() * 965.126,
        mc09 = random.random() * 965.126,
        mc10 = random.random() * 965.126,
        mc11 = random.random() * 965.126,
        mc12 = random.random() * 965.126,
        mc13 = random.random() * 965.126,
        mc14 = random.random() * 965.126,
        mc15 = random.random() * 965.126,
    )
    d.save()
