from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'^latest/$', views.latest),
    url(r'^initial/(?P<num_rows>\d+)/$', views.initial),
)
