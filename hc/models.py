from django.db import models

class data(models.Model):
    timestamp = models.DateTimeField(primary_key=True)
    mc01 = models.DecimalField(max_digits=14, decimal_places=4)
    mc02 = models.DecimalField(max_digits=14, decimal_places=4)
    mc03 = models.DecimalField(max_digits=14, decimal_places=4)
    mc04 = models.DecimalField(max_digits=14, decimal_places=4)
    mc05 = models.DecimalField(max_digits=14, decimal_places=4)
    mc06 = models.DecimalField(max_digits=14, decimal_places=4)
    mc07 = models.DecimalField(max_digits=14, decimal_places=4)
    mc08 = models.DecimalField(max_digits=14, decimal_places=4)
    mc09 = models.DecimalField(max_digits=14, decimal_places=4)
    mc10 = models.DecimalField(max_digits=14, decimal_places=4)
    mc11 = models.DecimalField(max_digits=14, decimal_places=4)
    mc12 = models.DecimalField(max_digits=14, decimal_places=4)
    mc13 = models.DecimalField(max_digits=14, decimal_places=4)
    mc14 = models.DecimalField(max_digits=14, decimal_places=4)
    mc15 = models.DecimalField(max_digits=14, decimal_places=4)
