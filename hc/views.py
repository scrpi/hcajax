import json
from django.http import HttpResponse

from models import data

def latest(request):
    row = data.objects.latest('timestamp')
    output = [
        float(row.mc01),
        float(row.mc02),
        float(row.mc03),
        float(row.mc04),
        float(row.mc05),
        float(row.mc06),
        float(row.mc07),
        float(row.mc08),
        float(row.mc09),
        float(row.mc10),
        float(row.mc11),
        float(row.mc12),
        float(row.mc13),
        float(row.mc14),
        float(row.mc15)
    ]
    return HttpResponse(json.dumps(output), content_type="application/json")

def initial(request, num_rows):
    rows = data.objects.order_by('-timestamp')[:num_rows]

    output = dict()

    for i in range(1, 16):
        key = "mc{}".format(str(i).zfill(2))
        output[key] = []

    for r in rows:
        output['mc01'].insert(0, float(r.mc01))
        output['mc02'].insert(0, float(r.mc02))
        output['mc03'].insert(0, float(r.mc03))
        output['mc04'].insert(0, float(r.mc04))
        output['mc05'].insert(0, float(r.mc05))
        output['mc06'].insert(0, float(r.mc06))
        output['mc07'].insert(0, float(r.mc07))
        output['mc08'].insert(0, float(r.mc08))
        output['mc09'].insert(0, float(r.mc09))
        output['mc10'].insert(0, float(r.mc10))
        output['mc11'].insert(0, float(r.mc11))
        output['mc12'].insert(0, float(r.mc12))
        output['mc13'].insert(0, float(r.mc13))
        output['mc14'].insert(0, float(r.mc14))
        output['mc15'].insert(0, float(r.mc15))

    return HttpResponse(json.dumps(output), content_type="application/json")
